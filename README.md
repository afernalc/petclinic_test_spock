
Pruebas de PetClinic con Selenium y Spock
=========================================

    
    
==== Especificación de las pruebas con Spock
Los tests se realizan utilizando el framework Spock, con una definición BDD
La especificación de los tests en groovy se realiza en la clase src/test/groovy/testing/petclinic/spock/PetclinicBasicTestSpec.groovy: 
 - En el método setupSpec se inicializan los distintos tipos denavegadores
 - En el método cleanupSpec se libera cierran los navegadores y se libera la conexión
 - Hay métodos específicos para cada test case, por ejemplo "visitHomePage" donde se especifica el comportamiento esperado de un modo intuitivo  
Se apoya en un objeto PetclinicAppTester (se declara de tipo shared para compartirlo en los casos de test de la suite porque su inicialización es relativamente costosa).     

==== Notas sobre la clase PetclinicAppTester
En el arranque de la clase PetclinicAppTester hay un atriubuto de tipo WebDriver por cada tipo de navegador (Chrome, Firefox) y se instancian en las lamadas al método setup, al que se le indica el tipo de navegador a inicializar.    

==== Ejecución de los tests
You can run the unit tests by using the following command:

    clean test -Dproperties_file="<<ruta_a_fichero_properties>>/mytest.properties"

==== Integración del resultado de los tests con otras herramientas 
Se incluye la posibilida de notificar el resultado de las pruebas a otras herramientas:
- InfluxDB
- JIRA Zepyr 

==== Integración con Zephyr
La integración con Zephyr está hecha, pero Pendiente de probar y documentar

==== Integración con InfluxDB
Requisito: crear BBD en influxDB, con nombre "seleniumTests" --> create database seleniumTests
Crea dos coleccione en influx:
- SeleniumTestsResults_counters.- Incluye los resultados "OK/KO" de cada ejecución de cada test
- SeleniumTestsResults.- Resultado OK/KO de cada ejecución de cada test  


COnfiguración en el properties

