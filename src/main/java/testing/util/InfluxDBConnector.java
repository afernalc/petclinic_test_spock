package testing.util;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Base64;

public class InfluxDBConnector {

	
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private String influxURL; // InfluxDB base URL
	private String influxPort; // InfluxDB port
	private String influxUsername; // InfluxDB username
	private String influxPwd; // InfluxDB user pwd
	private String influxDataBase; // InfluxDB Database
	private InfluxDB influxDB = null;
	
	public InfluxDBConnector (String influxURL, String influxPort, String influxUsername, String influxPwd, String influxDataBase) throws IOException {
		this.influxURL = influxURL;
		this.influxPort = influxPort;
		this.influxUsername = influxUsername;
		this.influxPwd = influxPwd;
		this.influxDataBase = influxDataBase;
	}
	
	// Connect to InfluxDB
	public boolean connect () throws IOException {
		logger.info("Sending measurements to InfluxDB ...");
		try {
			if ((influxUsername == null) || (influxUsername.isEmpty())) {
				influxDB = InfluxDBFactory.connect(influxURL);
			}
			else {
				influxDB = InfluxDBFactory.connect(influxURL, influxUsername, influxPwd);
			}
		}
		catch (Exception e) {
	        logger.error("ERROR Exception!" + e.getMessage());
	        e.printStackTrace();
		}
        return true;
    }
	
	// Send result to InfluxDB
	public boolean sendMeasurements(HashMap <String, String> measurements, String testSuiteName) throws IOException {
		logger.info("Sending measurements to InfluxDB ...");
		if (influxDB == null) {
			connect ();
		}
		int numberOfTests = measurements.size();
		int numberOfErrors = 0;
		long currentTimems = System.currentTimeMillis();
		
		// Write each individual testCase result (OK --> 1 / ERROR --> 0)
		Iterator<Entry<String, String>> it = measurements.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pair = (Map.Entry<String, String>) it.next();
			if (!pair.getValue().equalsIgnoreCase("OK")) {
				numberOfErrors = numberOfErrors + 1;
			}
            Point point = Point.measurement("SeleniumTestsResults")
				.time(currentTimems, TimeUnit.MILLISECONDS)
				.tag("testSuiteName", testSuiteName)
				.tag("testName", pair.getKey())
				.addField("success", pair.getValue().equalsIgnoreCase("OK")?Integer.valueOf(1):Integer.valueOf(-1))
				//.addField("success", pair.getValue())
				.build();
            influxDB.write(influxDataBase, "autogen", point);
		}
		// Write Totals
		Point countersPoint = Point.measurement("SeleniumTestsResults_counters")
				.time(currentTimems, TimeUnit.MILLISECONDS)
				.tag("testSuiteName", testSuiteName )
				.addField("executed_tests", Integer.valueOf(numberOfTests))
				.addField("passed_tests", Integer.valueOf(numberOfTests - numberOfErrors))
				.addField("failed_tests", Integer.valueOf(numberOfErrors))
				.build();
        influxDB.write(influxDataBase, "autogen", countersPoint);

		
        return true;
    }
	
	

}
