package testing.util;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import testing.util.TMConnectorZephyr;

public class TestConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
	private static TestConfiguration tcInstance = null;

	private Properties testGlobalProperties = null;
	private XSSFWorkbook xlsDatafile = null;
	private TMConnectorZephyr zephyrConnector = null;
	private InfluxDBConnector influxDBConnector = null;

	public static TestConfiguration getInstance() {
		if (tcInstance == null) {
			tcInstance = tcBuild();
		}
		return tcInstance;
	}

	private static TestConfiguration tcBuild() {
		TestConfiguration tc = null;
		Properties testGlobalProperties = null;
		XSSFWorkbook xlsDatafile = null;
		TMConnectorZephyr zephyrConnector = null;
		InfluxDBConnector influxDBConnector = null;
		boolean success = false;
		try {
			logger.info("Configuring global properties and utils ...");
			String propertiesFilename =System.getProperty("properties_file");

			// Read global properties file
			logger.info("Reading properties file [" + propertiesFilename + "] ...");
			testGlobalProperties = testing.util.ConfigReader.readConfigurationFile(propertiesFilename);

			// Read XLS Test data file
			String XLSfilename = testGlobalProperties.getProperty("XLSfilename");
			logger.info("Reading XLS file [" + XLSfilename + "] ...");
			xlsDatafile = testing.util.XLSReader.createWB (XLSfilename);

			// Configure Zephyr connector
			String zephyrIntegration = testGlobalProperties.getProperty("zephyrIntegration");
			if ((zephyrIntegration!= null) && (zephyrIntegration.equals("YES"))) {
				logger.info("Zephyr connector is enabled. Test results will be notified to Zephyr.");
				logger.info("Reading Zephyr connector configuration ...");
				String zephyrBaseURL = testGlobalProperties.getProperty("zephyrBaseURL");
				String zephyrUsername = testGlobalProperties.getProperty("zephyrUsername");
				String zephyrUserpwd = testGlobalProperties.getProperty("zephyrUserpwd");
				String zephyrProjectKey = testGlobalProperties.getProperty("zephyrProjectKey");
				String zephyrCycleId = testGlobalProperties.getProperty("zephyrCycleId");
				zephyrConnector = new TMConnectorZephyr(zephyrBaseURL, zephyrUsername, zephyrUserpwd, zephyrProjectKey, zephyrCycleId);
			}
			else {
				logger.info("Zephyr connector is disabled. Test results wont be notified to Zephyr.");
			}

			// Configure InfluxDB connector
			String influxDBIntegration = testGlobalProperties.getProperty("influxDBIntegration");
			if ((influxDBIntegration!= null) && (influxDBIntegration.equals("YES"))) {
				logger.info("InfluxDB connector is enabled. Test results will be notified to InfluxDB.");
				logger.info("Reading InfluxDB connector configuration ...");
				
				String influxURL = testGlobalProperties.getProperty("influxURL");
				String influxPort = testGlobalProperties.getProperty("influxPort");
				String influxUsername = testGlobalProperties.getProperty("influxUsername");
				String influxPwd = testGlobalProperties.getProperty("influxPwd");
				String influxDataBase = testGlobalProperties.getProperty("influxDataBase");

				influxDBConnector = new InfluxDBConnector(influxURL, influxPort, influxUsername, influxPwd, influxDataBase);
			}
			else {
				logger.info("InfluxDB connector is disabled. Test results wont be notified to InfluxDB.");
			}

			// Create global configuration object
			tc = new TestConfiguration(testGlobalProperties, xlsDatafile, zephyrConnector, influxDBConnector);


		} // end try
		catch (IOException e) {
			logger.error("ERROR IOException!" + e.getMessage());
			success = false;
		}
		catch (Exception e) {
			logger.error("ERROR Exception!" + e.getMessage());
			success = false;
		}
		return tc;
	}

	public TestConfiguration (Properties testGlobalProperties, XSSFWorkbook xlsDatafile, TMConnectorZephyr zephyrConnector, InfluxDBConnector influxDBConnector) {
		this.testGlobalProperties = testGlobalProperties; 
		this.xlsDatafile = xlsDatafile; 
		this.zephyrConnector = zephyrConnector;
		this.influxDBConnector = influxDBConnector;
	}
	
	public Properties getTestGlobalProperties() {
		return this.testGlobalProperties;
	}

	public XSSFWorkbook getXlsDatafile() {
		return this.xlsDatafile;
	}

	public TMConnectorZephyr getZephyrConnector() {
		return this.zephyrConnector;
	}

	public InfluxDBConnector getInfluxDBConnector() {
		return this.influxDBConnector;
	}

}
