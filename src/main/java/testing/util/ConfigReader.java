package testing.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigReader {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	public static Properties readConfigurationFile(String filename) {
		Properties prop = new Properties();

		try {
		    prop.load(new FileInputStream(filename));
		} catch (IOException e) {
			logger.error("IOException:" + e.getMessage());
		}
		return prop;
	}
}