package testing.petclinic.spock;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import testing.util.TestConfiguration;
import testing.util.XLSReader;
import java.util.HashMap;

//import java.util.Properties;
//import java.io.IOException;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PetclinicAppTester {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
    private WebDriver driverFirefox = null;
    private WebDriver driverChrome = null;
    
    // Global testsuite status. Hash Table that contains one entry for each testcase. Can be used to report global results at the end of the execution 
    private HashMap<String, String> testSuiteStatus = new HashMap<String, String>(); 
    
    
    private static String PETCLINIC_TESTSUITE1_TABNAME = "Peticlinic_TestSuite_1";
    private static String PETCLINIC_TESTSUITE1_HOMEPAGE_TESTNAME = "Petclinic_Home";
    private static String PETCLINIC_TESTSUITE1_GETPETS_TESTNAME = "Petclinic_GetPets";

    public boolean setup(String browser) throws Exception{
        logger.info("Setting up Remote Web Driver ( " + browser + ") ...");
    	
        DesiredCapabilities capabilities = null;
        boolean success = true;
        //Check if parameter passed from TestNG is 'firefox'
        if(browser.equalsIgnoreCase("firefox")){
            //create firefox instance
            capabilities = DesiredCapabilities.firefox();
        }
        //Check if parameter passed as 'chrome'
        else if(browser.equalsIgnoreCase("chrome")){
            capabilities = DesiredCapabilities.chrome();
        }
        //Check if parameter passed as 'Edge'
        else if(browser.equalsIgnoreCase("Edge")){
            capabilities = DesiredCapabilities.edge();
        }
        else{
            //If no browser passed throw exception
        	success = false;
            throw new Exception("Browser is not correct");
        }
        try {

            //DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            //DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability("version", "");
            capabilities.setPlatform(Platform.LINUX);
            logger.info("Starting Remote Web Driver ...");
            String webdriver_URL = TestConfiguration.getInstance().getTestGlobalProperties().getProperty("webdriver_URL");
            logger.info("Connecting to Selenium Hub at [" + webdriver_URL + "] ...");
            
            if(browser.equalsIgnoreCase("firefox")){
            	driverFirefox = new RemoteWebDriver(new URL(webdriver_URL), capabilities);
            }
            else if(browser.equalsIgnoreCase("chrome")){
            	driverChrome = new RemoteWebDriver(new URL(webdriver_URL), capabilities);
            }
            else {
            	success = false;
            }
            logger.info("Starting browser ...");
        } catch (RuntimeException e) {
        	success = false;
            logger.error("ERROR Runtime!" + e.getMessage());
        } catch (MalformedURLException e) {
        	success = false;
            logger.error("ERROR MalformedURLException!" + e.getMessage());
        }
        return success;
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    
    public boolean driverIsAlive(String browser) {
        if(browser.equalsIgnoreCase("firefox")){
        	return driverFirefox != null;
        }
        else if(browser.equalsIgnoreCase("chrome")){
        	return driverChrome != null;
        }
        return false;
    	
    }

    public boolean firefoxDriverIsAlive() {
    	return driverFirefox != null;
    }
    
    public void freeDriver(String browser) throws Exception {
        //close browser
    	
        if(browser.equalsIgnoreCase("firefox")){
            if (driverFirefox != null) {
                logger.info("Closing SeleniumWeb Firefox driver ...");
                //driverFirefox.close();
                driverFirefox.quit();
                logger.info("SeleniumWeb Firefox driver closed.");
            }        
        }
        else if(browser.equalsIgnoreCase("chrome")){
            if (driverChrome != null) {
                logger.info("Closing SeleniumWeb Chrome driver ...");
                //driverChrome.close();
                driverChrome.quit();
                logger.info("SeleniumWeb Chrome driver closed.");
            }        
        }
    }

    public void afterClass() {
        //driver.quit();
    }

    public String browseHomePage(String browser) {
        logger.info("Browsing Home page ...");
        // declaration and instantiation of objects/variables
        String currentTitle = null;
        
        WebDriver driver = null;
        if(browser.equalsIgnoreCase("chrome")) {
        	driver = driverChrome;
        }
        else {
        	driver = driverFirefox;
        }
        
        try {
            // Read test data
            logger.info("Reading Test data ...");
            String tabName = PETCLINIC_TESTSUITE1_TABNAME;
            String testName = PETCLINIC_TESTSUITE1_HOMEPAGE_TESTNAME;
            String baseUrl = XLSReader.getParameter(TestConfiguration.getInstance().getXlsDatafile(), tabName, testName, "baseUrl");
            //String expectedText = XLSReader.getParameter(TestConfiguration.getInstance().getXlsDatafile(), tabName, testName, "expectedText");

            logger.info("URL: " + baseUrl);
            // launch Browser and direct it to the Base URL
            driver.get(baseUrl);
            logger.info("Checking response ...");
            //String pageSource = driver.getPageSource();
            currentTitle = driver.getTitle();
            logger.info("Retrieved Page Title is " + currentTitle );

        } catch (RuntimeException e) {
            logger.error("ERROR Runtime!" + e.getMessage());
        }
        return currentTitle;
    }
    
    
    
    public String getHomepageExpectedTitle() {
    	logger.info("getting homepage expected title from XLS datafile...");
    	String expectedTitle = null;
        try {
            // Read test data
            String tabName = "Peticlinic_TestSuite_1";
            String testName = "Petclinic_Home";
            expectedTitle = XLSReader.getParameter(TestConfiguration.getInstance().getXlsDatafile(), tabName, testName, "expectedTitle");
        } catch (RuntimeException e) {
	        logger.error("ERROR Runtime!" + e.getMessage());
        }
        return expectedTitle;
    }
    
    public void notifyHomePageTestResult (boolean success) {

		if (success) {
			// If test fails with one browser, its marked as failed
			if (!testSuiteStatus.containsKey(PETCLINIC_TESTSUITE1_HOMEPAGE_TESTNAME)) {
				testSuiteStatus.put(PETCLINIC_TESTSUITE1_HOMEPAGE_TESTNAME, "OK");
			}
    		logger.info("Notifying HomePageTestResult - Result OK");
		}
    	else {
    		testSuiteStatus.put(PETCLINIC_TESTSUITE1_HOMEPAGE_TESTNAME, "KO");
    		logger.info("Notifying HomePageTestResult - Result KO");
    	}

        // notify results to Zephyr
        if (TestConfiguration.getInstance().getZephyrConnector() != null) {
            TestConfiguration.getInstance().getZephyrConnector().notifyTestResult(PETCLINIC_TESTSUITE1_TABNAME, PETCLINIC_TESTSUITE1_HOMEPAGE_TESTNAME, success);
        }
        
        
    }
    
    public String browseGetPets (String browser) {
        logger.info("Browsing Home page ...");
        // declaration and instantiation of objects/variables
        String currentContent = null;
        
        WebDriver driver = null;
        if(browser.equalsIgnoreCase("chrome")) {
        	driver = driverChrome;
        }
        else {
        	driver = driverFirefox;
        }
        
        try {
            // Read test data
            logger.info("Reading Test data ...");
            String tabName = PETCLINIC_TESTSUITE1_TABNAME;
            String testName = PETCLINIC_TESTSUITE1_GETPETS_TESTNAME;
            String baseUrl = XLSReader.getParameter(TestConfiguration.getInstance().getXlsDatafile(), tabName, testName, "baseUrl");

            logger.info("URL: " + baseUrl);
            // launch Browser and direct it to the Base URL
            driver.get(baseUrl);
            logger.info("Checking response ...");
            String pageSource = driver.getPageSource();
            //logger.info("Retrieved Page content is " + pageSource );

        } catch (RuntimeException e) {
            logger.error("ERROR Runtime!" + e.getMessage());
        }
        return currentContent;
    }

    public String getPetsExpectedContent() {
    	logger.info("getting pets REST Service expected content from XLS datafile...");
    	String expectedContent = null;
        try {
            // Read test data
            String tabName = "Peticlinic_TestSuite_1";
            String testName = "Petclinic_GetPets";
            expectedContent = XLSReader.getParameter(TestConfiguration.getInstance().getXlsDatafile(), tabName, testName, "expectedText");
        } catch (RuntimeException e) {
	        logger.error("ERROR Runtime!" + e.getMessage());
        }
        return expectedContent;
    }


    public void notifyGetPetsTestResult (boolean success) {
    	if (success) {
    		logger.info("Notifying GetPetsTestResult - Result OK");
    		// If test fails with one browser, its marked as failed
    		if (!testSuiteStatus.containsKey(PETCLINIC_TESTSUITE1_GETPETS_TESTNAME)) {
    			testSuiteStatus.put(PETCLINIC_TESTSUITE1_GETPETS_TESTNAME, "OK");
    		}
    	}
    	else {
    		testSuiteStatus.put(PETCLINIC_TESTSUITE1_GETPETS_TESTNAME, "KO");
    		logger.info("Notifying GetPetsTestResult - Result KO");
    	}

        // notify results to Zephyr
        if (TestConfiguration.getInstance().getZephyrConnector() != null) {
            TestConfiguration.getInstance().getZephyrConnector().notifyTestResult(PETCLINIC_TESTSUITE1_TABNAME, PETCLINIC_TESTSUITE1_GETPETS_TESTNAME, success);
        }
        
    }
    
    // Returns global TestSuite status: one entry for each test casse with the associated result: OK/ERROR
    public HashMap<String, String> getTestSuiteStatus () {
    	return testSuiteStatus;
    }
    
    public void notifyResultsToInfluxDB() {
        if (TestConfiguration.getInstance().getInfluxDBConnector() != null) {
            try {
            	TestConfiguration.getInstance().getInfluxDBConnector().sendMeasurements(testSuiteStatus, "Petclinic Test Suite");
            }
            catch (IOException e) {
    	        logger.error("ERROR Runtime!" + e.getMessage());
            }
        }
    	
    }
    
    
    
}

