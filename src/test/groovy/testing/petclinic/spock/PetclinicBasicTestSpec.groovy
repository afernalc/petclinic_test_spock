package testing.petclinic.spock

import spock.lang.Specification
import spock.lang.Shared
import spock.lang.Unroll

/**
 * @author Minsait
 */
class PetclinicBasicTestSpec extends Specification {

    @Shared petclinicAppTester = new PetclinicAppTester();

   
	@Unroll 
	def "visitHomePage - Title of home page Using #browserType should be Petclinic application name"() {  
        given: "PetClinic Web App is running"
		when: "Browser points to the Home Page"	          
        	def pageTitle = petclinicAppTester.browseHomePage(browserType)
        	def expectedTitle = petclinicAppTester.getHomepageExpectedTitle()
        	def driverIsAlive = petclinicAppTester.driverIsAlive(browserType)
        	
        then: "#browserType Web Driver is Alive"
        	driverIsAlive == true  
        and: "Title should include the Petclinic application name"
	        // Notify test result to integrated tools (InfluxDB, Zepyr) 
	        petclinicAppTester.notifyHomePageTestResult(pageTitle == expectedTitle)
	        pageTitle == expectedTitle
		where: "Browser type is #browserType"  
	    	browserType ||expectedResult  
	    	"firefox" || "XXX"
	    	"chrome" || "XXX" 
	}  

	@Unroll 
	def "getPets Service"() {  
        given: "PetClinic Web App is running"
		when: "Browser points to the getPets API Service"	          
        	def pageContent = petclinicAppTester.browseGetPets(browserType)
        	def expectedContent = petclinicAppTester.getPetsExpectedContent()
        	def driverIsAlive = petclinicAppTester.driverIsAlive(browserType)
        	
        then: "#browserType Web Driver is Alive"
        	driverIsAlive == true  
        and: "Title should include the Petclinic application name"
	        // Notify test result to integrated tools (InfluxDB, Zepyr) 
	        petclinicAppTester.notifyGetPetsTestResult(pageContent == expectedContent)
	        pageContent == expectedContent
		where: "Browser type is #browserType"  
	    	browserType ||expectedResult  
	    	"firefox" || "XXX"
	    	"chrome" || "XXX" 
	}     
    
    def setupSpec() {
        println "Setup specification"
        petclinicAppTester.setup("firefox")
        petclinicAppTester.setup("chrome")
    }
 
    def cleanupSpec() {
    	// Report results to InfluxDB
		petclinicAppTester.notifyResultsToInfluxDB()
		// Free browsers    
    	def myBrowser = "firefox"
    	petclinicAppTester.freeDriver("firefox")
    	petclinicAppTester.freeDriver("chrome")
        println "Clean up specification"
    }    
}
